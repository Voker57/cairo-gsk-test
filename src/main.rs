use gtk::glib;
use gtk::graphene;
use gtk::gsk;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
glib::wrapper! {
	pub struct MyWidget(ObjectSubclass<Imp>) @extends gtk::Widget;
}
#[derive(Default)]
pub struct Imp;

#[glib::object_subclass]
impl ObjectSubclass for Imp {
	const NAME: &'static str = "MyWidget";
	type Type = MyWidget;
	type ParentType = gtk::Widget;
	type Interfaces = ();
}

impl ObjectImpl for Imp {}

impl Default for MyWidget {
	fn default() -> Self {
		Self::new()
	}
}

impl WidgetImpl for Imp {
	fn snapshot(&self, _widget: &Self::Type, snapshot: &gtk::Snapshot) {
		let node = gsk::CairoNode::new(&graphene::Rect::new(0., 0., 100., 100.));
		let ctx = node.draw_context();
		ctx.set_source_rgba(0., 0., 0., 1.);
		// Align top left
		ctx.set_line_width(2.0);

		ctx.move_to(50., 50.);
		ctx.fill().unwrap();
		ctx.line_to(20., 20.);
		snapshot.append_node(node);
	}
}

impl MyWidget {
	pub fn new() -> Self {
		glib::Object::new(&[]).expect("Failed to create a MyWidget")
	}
}

fn main() {
	let application = gtk::Application::new(Some("com.github.gtk-rs.examples.paintable"), Default::default());
	application.connect_activate(build_ui);
	application.run();
}

fn build_ui(application: &gtk::Application) {
	let window = gtk::ApplicationWindow::new(application);
	window.set_title(Some("Custom Paintable"));
	window.set_default_size(500, 500);

	let mw = crate::MyWidget::new();

	window.set_child(Some(&mw));

	window.show();
}
